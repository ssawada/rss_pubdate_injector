package rsspubdateinjector

import (
	"encoding/xml"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
)

const (
	RFC1123M  = "Mon, _2 Jan 2006 15:04:05 MST"
	RFC1123MZ = "Mon, _2 Jan 2006 15:04:05 -0700"
)

func init() {
	http.HandleFunc("/", injector)
}

type Time struct {
	time.Time
}

func (t *Time) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var s string
	err := d.DecodeElement(&s, &start)
	if err != nil {
		return err
	}
	parse, err := time.Parse(time.RFC1123, s)
	if err == nil {
		*t = Time{parse}
		return nil
	}
	parse, err = time.Parse(time.RFC1123Z, s)
	if err == nil {
		*t = Time{parse}
		return nil
	}
	parse, err = time.Parse(RFC1123M, s)
	if err == nil {
		*t = Time{parse}
		return nil
	}
	parse, err = time.Parse(RFC1123MZ, s)
	if err == nil {
		*t = Time{parse}
		return nil
	}
	return err
}

func (t Time) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(t.Format(time.RFC1123Z), start)
}

type RSS2 struct {
	XMLName        xml.Name `xml:"rss"`
	Version        string   `xml:"version,attr"`
	Title          string   `xml:"channel>title"`
	Link           string   `xml:"channel>link"`
	Description    string   `xml:"channel>description"`
	Language       string   `xml:"channel>language"`
	PubDate        Time     `xml:"channel>pubDate"`
	LastBuildDate  Time     `xml:"channel>lastBuildDate"`
	ManagingEditor string   `xml:"channel>managingEditor"`
	Image          Image    `xml:"channel>image"`
	ItemList       []Item   `xml:"channel>item"`
}

type Image struct {
	Title string `xml:"title"`
	URL   string `xml:"url"`
	Link  string `xml:"link"`
}

type Item struct {
	Title       string        `xml:"title"`
	Link        string        `xml:"link"`
	Description template.HTML `xml:"description"`
	GUID        GUID          `xml:"guid"`
	PubDate     Time          `xml:"pubDate"`
}

type GUID struct {
	CharData    string `xml:",chardata"`
	IsPermaLink bool   `xml:"isPermaLink,attr"`
}

func injector(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	resp, err := client.Get("http://arxiv.org/rss/cs.CL?version=2.0")
	if resp != nil {
		defer func() {
			io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for k, s := range resp.Header {
		for _, v := range s {
			w.Header().Add(k, v)
		}
	}

	var c RSS2
	d := xml.NewDecoder(resp.Body)
	err = d.Decode(&c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for i := range c.ItemList {
		c.ItemList[i].PubDate = c.PubDate
	}

	e := xml.NewEncoder(w)
	err = e.Encode(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
